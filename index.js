var through = require("through2");
var dotted = require("dotted");

module.exports = function(params)
{
    // Normalize the parameters.
    params = params || {};
    params.property = params.property || "availability";
    params.search = params.search || "private";

    // Set up and return the pipe.
    var pipe = through.obj(
        function(file, encoding, callback)
        {
            // See if we have the value requested. If we don't, then we stop
            // processing since there is nothing to do.
            var value = dotted.getNested(file, params.property);

            if (!value)
            {
                return callback(null, file);
            }

            // See if it matches the value, if it does, then skip it.
            if (value.indexOf(params.search) >= 0)
            {
                if (file.path.indexOf("mummy-girl/chapter-03") >= 0)
                console.log(file.path, file.page, params.property, value, value.indexOf(params.search));

                return callback();
            }

            // Add the file back into the stream.
            callback(null, file);
        });

    return pipe;
}
